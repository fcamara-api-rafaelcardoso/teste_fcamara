#language:pt

Funcionalidade: Consulta cep

Esquema do Cenario: Consultar servico de consulta de ceps 
    Dado que que realizo um request na API de consulta de ceps '<Cenario>'
    Quando valido os dados do retorno da Requisicao '<Cenario>'
    Entao valido o teste de acordo com os parametros estabelecidos na massa '<Cenario>'
Exemplos:
|Cenario|
|valido|
|invalido|
