require 'report_builder'

at_exit do
    ReportBuilder.configure do |config|
      t = Time.now
      t.to_s
      config.input_path = 'data/reports'
      config.report_path = 'data/reports/Consulta de CEP ' + t.strftime('%d_%m_%y') + ' ' + t.strftime('%H_%M_%S')
      config.report_types = [:html]
      config.report_title = 'Entrevista Bruna  '
      config.additional_info = { browser: 'Chrome', environment: 'Stage 5', Data_execução: t.strftime('%d/%m/%y'), Horário_Conclusão: t.strftime('%H:%M:%S') }
      config.include_images = true
    end
    ReportBuilder.build_report

#    driver.driver_quit

  end
