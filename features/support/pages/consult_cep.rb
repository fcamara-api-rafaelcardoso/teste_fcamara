  require 'httparty'
  require 'pry'
  require 'test/unit'
  require 'rubygems'


class Cep_Controller

#Realiza o request na URI API
def sendRequest(uri_cep)

binding.pry
   response = HTTParty.get("https://viacep.com.br/ws/#{MD[uri_cep]['path_complement']}/json/")

   #Retorna o Response contendo todos os parametros do retorno do request
   return response
end


# valida todos as tags presentes no body da requisição
def valid_body_request(response, massa)


  fields_request = response.to_hash
  #fields_request.split(",")

  binding.pry

#caso o retorno da requisicao seja OK 200 - valida os itens presentes no body
    if response.code.to_s == MD['valido']['code_expected']

        MD['valido']['parameters_body']['cep'].include? fields_request['cep']
        MD['valido']['parameters_body']['logradouro'].include? fields_request['logradouro']
        MD['valido']['parameters_body']['complemento'].include? fields_request['complemento']
        MD['valido']['parameters_body']['bairro'].include? fields_request['bairro']
        MD['valido']['parameters_body']['localidade'].include? fields_request['localidade']
        MD['valido']['parameters_body']['ibge'].include? fields_request['ibge']
        MD['valido']['parameters_body']['gia'].include? fields_request['gia']
        MD['valido']['parameters_body']['ddd'].include? fields_request['ddd']
        MD['valido']['parameters_body']['siafi'].include? fields_request['siafi']

        puts "itens body validado"

      else

        puts "Cep informado não é valido"
    end


  #puts response.headers.inspect
  #puts response.code
  #puts response.body
  #puts response.message
  #p response.success?


end


def valid_response_code(response, massa)


    if response.code.to_s == MD['valido']['code_expected']
        puts "Requisicao 200 OK"

      else
          puts "Falha na requisição cep invalido"

        end
      end


end
